#ELSŐ MEGOLDÁS:
#A HTML FÁJLBAN A SZÁMOK EGYMÁS MELLETT JELENNEK MEG

# with open("output.txt", "r") as output_data:
#     body = output_data.read()

# text = '''<!DOCTYPE html>
# <html lang="en">
# <head>
#     <meta charset="UTF-8">
#     <meta http-equiv="X-UA-Compatible" content="IE=edge">
#     <meta name="viewport" content="width=device-width, initial-scale=1.0">
#     <title>Document's title</title>
# </head>
# <body>
# <h1>Output numbers:</h1>
# {}
# </body>
# </html>'''.format(body)

# with open("index.html", "w") as index_html:
#     index_html.write(text)

# print("html file is done")




#MÁSODIK MEGOLDÁS
#A HTML FÁJLBAN A SZÁMOK EGYMÁS ALATT JELENNEK MEG

with open("output.txt", "r") as output_data:
    body = output_data.read().replace("\n", "<br>")
    turn_list = body.split()
    html_body = "".join(turn_list)

text = '''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document's title</title>
</head>
<body>
<h1>Output numbers:</h1>
{}
</body>
</html>'''.format(html_body)

with open("index.html", "w") as index_html:
    index_html.write(text)

print("html file is done")
