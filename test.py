inputs = []
outputs = []
index = 0

with open("input.txt", "r") as input_file:
    for i in input_file:
        x = int(i)
        inputs.append(x)
print(inputs)

with open("output.txt", "r") as output_file:
    for i in output_file:
        x = int(i)
        outputs.append(x)
print(outputs)

for i,j in zip(inputs, outputs):
    if i*2 == j:
        index += 1
    else:
        print("Exit code 1!")
        exit(1)
print("Test successful!")
