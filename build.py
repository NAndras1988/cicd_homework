numbers = []
multiplied_numbers = []

with open("input.txt", "r") as input:
    for i in input:
        turn_to_int = int(i)
        numbers.append(turn_to_int)
    for i in numbers:
        multiply = i*2
        str_multiple = str(multiply)
        multiplied_numbers.append(str_multiple)

with open("output.txt", "w") as output:
    for i in multiplied_numbers:
        output.write("%s\n" % i)

print("Build successful!")
